
#import "RNImageQr.h"
@import Vision;

@implementation RNImageQr

- (dispatch_queue_t)methodQueue
{
    return dispatch_get_main_queue();
}
RCT_EXPORT_MODULE()


static NSString *const detectionNoResultsMessage = @"Something went wrong";

RCT_REMAP_METHOD(scanQRImage,
                 scanQRImage:(NSDictionary *)params
                 resolver:(RCTPromiseResolveBlock)resolve
                 rejecter:(RCTPromiseRejectBlock)reject) {
    NSString *imagePath = [params valueForKey:@"imageURL"];
    if (!imagePath) {
        resolve(@NO);
        return;
    }
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        CIImage *ciImage;
        if ([[params valueForKey:@"isLocalFile"] boolValue]) {
            NSData *imageData = [NSData dataWithContentsOfFile:imagePath];
            ciImage = [[CIImage alloc] initWithData:imageData];
        } else {
            ciImage = [[CIImage alloc] initWithContentsOfURL:[[NSURL alloc] initWithString:imagePath]];
        }
        NSDictionary* options;
        CIContext* context = [CIContext context];
        options = @{ CIDetectorAccuracy : CIDetectorAccuracyHigh };
        
        CIDetector* qrDetector = [CIDetector detectorOfType:CIDetectorTypeQRCode
                                                    context:context
                                                    options:options];
        if ([[ciImage properties] valueForKey:(NSString*) kCGImagePropertyOrientation] == nil) {
            options = @{ CIDetectorImageOrientation : @1};
        } else {
            options = @{ CIDetectorImageOrientation : [[ciImage properties] valueForKey:(NSString*) kCGImagePropertyOrientation]};
        }
        
        NSArray *features = [qrDetector featuresInImage:ciImage
                                                options:options];
        if (features == nil || features.count == 0) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                resolve(@{
                          @"errorKey": @3,
                          @"errorMsg": @"QR code not found in image",
                          });
            });
            return;
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            CIQRCodeFeature* qrFeature = features[0];
            resolve(@{
                      @"text": qrFeature.messageString
                      });
        });
    });
    
}

@end

