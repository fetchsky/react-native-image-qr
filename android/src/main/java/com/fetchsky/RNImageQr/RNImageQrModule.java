
package com.fetchsky.RNImageQr;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.WritableMap;
import com.google.zxing.*;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.DecoderResult;
import com.google.zxing.common.DetectorResult;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.decoder.Decoder;
import com.google.zxing.qrcode.detector.Detector;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.Map;

public class RNImageQrModule extends ReactContextBaseJavaModule {

  private final ReactApplicationContext reactContext;
  private ReadableMap errorsMap = null;
  private static final String IMAGE_URL = "imageURL";

  public RNImageQrModule(ReactApplicationContext reactContext) {
    super(reactContext);
    this.reactContext = reactContext;
    errorsMap = new Errors().ErrorMsgs;
  }

  @ReactMethod
  public void scanQRImage(final ReadableMap params, final Promise promise) {
    if(!params.hasKey(IMAGE_URL)){
      promise.reject("1", errorsMap.getString("1"));
      return;
    }
    final String imageURL = params.getString(IMAGE_URL);
    final int width = params.hasKey("width") ? params.getInt("width") : 400;
    final int height = params.hasKey("height") ? params.getInt("height") : 400;

    new AsyncTask<Void, Void, WritableMap>() {
      protected WritableMap doInBackground(Void... params) {
        WritableMap output = Arguments.createMap();
        Bitmap image;
        if (imageURL.contains("http")) {
          image = Utils.getBitmapFromURL(imageURL);
        } else {
          image = Utils.getBitmapFromFile(imageURL);
        }

        if (image == null) {
          output.putInt("errorKey", 2);
          output.putString("errorMsg", errorsMap.getString("2"));
          return output;
        }

        try {
          Bitmap resizedImage = Bitmap.createScaledBitmap(image, width, height, false);

          int[] intArray = new int[resizedImage.getWidth() * resizedImage.getHeight()];

          resizedImage.getPixels(intArray, 0, resizedImage.getWidth(), 0, 0, resizedImage.getWidth(), resizedImage.getHeight());
          LuminanceSource source = new RGBLuminanceSource(resizedImage.getWidth(), resizedImage.getHeight(), intArray);

          Decoder decoder = new Decoder();
          HybridBinarizer hb = new HybridBinarizer(source);
          BitMatrix bm = null;
          bm = hb.getBlackMatrix();
          Detector detector = new Detector(bm);
          Map<DecodeHintType, Object> tmpHintsMap = new EnumMap<DecodeHintType, Object>(
                  DecodeHintType.class);
          tmpHintsMap.put(DecodeHintType.TRY_HARDER, Boolean.TRUE);
          tmpHintsMap.put(DecodeHintType.POSSIBLE_FORMATS,
                  EnumSet.allOf(BarcodeFormat.class));
          tmpHintsMap.put(DecodeHintType.PURE_BARCODE, Boolean.FALSE);

          DetectorResult dResult = detector.detect(tmpHintsMap);

          if (dResult != null) {
            BitMatrix QRImageData = dResult.getBits();
            DecoderResult result = decoder.decode(QRImageData, tmpHintsMap);
            String text = result.getText();
            output.putString("text", text);
            return output;
          }
        } catch (NotFoundException e) {
          output.putInt("errorKey", 3);
          output.putString("errorMsg", errorsMap.getString("3"));
        } catch (FormatException e) {
          output.putInt("errorKey", 4);
          output.putString("errorMsg", errorsMap.getString("4"));
        } catch (ChecksumException e) {
          output.putInt("errorKey", 5);
          output.putString("errorMsg", errorsMap.getString("5"));
        } catch (Exception e) {
          output.putInt("errorKey", 6);
          output.putString("errorMsg", errorsMap.getString("6"));
        }
        return output;
      }
      protected void onPostExecute(WritableMap result) {
        promise.resolve(result);
      }
    }.execute();
  }


  @Override
  public String getName() {
    return "RNImageQr";
  }
}