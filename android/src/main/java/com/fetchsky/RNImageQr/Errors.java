package com.fetchsky.RNImageQr;

import android.util.ArrayMap;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.WritableMap;

import java.util.HashMap;
import java.util.Map;

public class Errors {
    WritableMap ErrorMsgs = Arguments.createMap();

    Errors(){
        ErrorMsgs.putString("1", "Image URL not found");
        ErrorMsgs.putString("2", "Image not found");
        ErrorMsgs.putString("3", "QR code not found in image");
        ErrorMsgs.putString("4", "Checksum exception");
        ErrorMsgs.putString("5", "Format exception");
        ErrorMsgs.putString("6", "Unknown error");
    }
}
